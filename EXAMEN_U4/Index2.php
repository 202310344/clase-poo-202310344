<?php
//Realizar dos clases, una abstracta y una clase que herede sus metodos - 40%

abstract class TiendaRopa{ 

    abstract protected function mercancia(); 

}
    class Prenda1 extends TiendaRopa{ 

        protected function almacen(){ 
            echo "Precio = $69"; 
        }
    }
    class Prenda1 extends TiendaRopa{

        protected function almacen(){ 
            echo "Tipo de prenda = Blusa"; 
        }
    }
    class Prenda2 extends TiendaRopa{ 

        protected function almacen(){ 
            echo "Precio = $100"; 
        }
    }
    class Prenda2 extends TiendaRopa{

        protected function almacen(){ 
            echo "Tipo de prenda = Camisa"; 
        }
    }
    $obj = new Prenda1();
    $obj->almacen();

    $obj = new Prenda2();
    $obj->almacen();
?>
