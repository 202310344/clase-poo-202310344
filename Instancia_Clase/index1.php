<?php

class MisGatos {
	function maullar(){
		print "Miauu Miauu Miauu <br>";	
	}
}

//Instanciamos objetos de la clase MisGatos
$gato1 = new MisGatos();
$gato2 = new MisGatos();


//Llamamos a los metodos para su impresion en el navegador
print "Gato 1 dice: ";
$gato1->maullar();
print "Gato 2 dice: ";
$gato2->maullar();
?>

