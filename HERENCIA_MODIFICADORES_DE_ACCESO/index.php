<?php

class EjemploModificadores{

    public function metodo1(){
        echo "este es el metodo 1";
    }
    private function metodo2(){
        echo "este es el metodo 2";
    }
    protected function metodo3(){
        echo "este es el metodo 3";
    }
    public function AccesoMetodo2(){
        echo "este es el metodo 1";
    }
}
class Hijo extends EjemploModificadores{
    public function AccesoMetodo3(){
        $this->metodo3();
    }
}
$obj - new Hijo;
$obj->metodo1();
$obj->AccesoMetodo3();
$obj2 - new EjemploModificadores;
$obj->AccesoMetodo2(); 

?>